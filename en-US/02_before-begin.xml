<?xml version="1.0"?>
<chapter><title>Before you begin</title>
<section><title>What do you want this system for?</title>

<para>Securing Debian is not very different from securing any other
system; in order to do it properly, you must first decide what you
intend to do with it. After this, you will have to consider that the
following tasks need to be taken care of if you want a really secure
system. </para>

<para>You will find that this manual is written from the bottom
up, that is, you will read some information on tasks to do before,
during and after you install your Debian system. The
tasks can also be thought of as:</para>

<itemizedlist>

    <listitem><para>Decide which services you need and limit your system to those.
This includes deactivating/uninstalling unneeded services, and adding
firewall-like filters, or tcpwrappers.</para></listitem>

<listitem><para>Limit users and permissions in your system. </para></listitem>

<listitem><para>Harden offered services so that, in
the event of a service compromise, the impact to your system is
minimized.</para></listitem>

<listitem><para>Use appropriate tools to guarantee that unauthorized
use is detected so that you can take appropriate measures.</para></listitem>

</itemizedlist>
</section>

<section id="securityreferences"><title>Be aware of general security problems</title>

<para>The following manual does not (usually) go into the details on why
some issues are considered security risks. However, you might want to
have a better background regarding general UNIX and (specific) Linux
security. Take some time to read over security related documents in
order to make informed decisions when you are encountered with
different choices. Debian GNU/Linux is based on the Linux kernel, so
much of the information regarding Linux, as well as from other
distributions and general UNIX security also apply to it (even if the
tools used, or the programs available, differ).</para>

<para>Some useful documents include:</para>

<itemizedlist>

<listitem><para>The <ulink name="Linux Security HOWTO"
url="http://www.tldp.org/HOWTO/Security-HOWTO/" />
is one of the
best references regarding general Linux security.</para></listitem>

<listitem><para>The <ulink name="Security Quick-Start HOWTO for Linux" 
url="http://www.tldp.org/HOWTO/Security-Quickstart-HOWTO/" />
is also a very good starting point for novice users (both to Linux
and security).</para></listitem>

<listitem><para>The <ulink url="http://seifried.org/lasg/" name="Linux Security
Administrator's Guide" /> is a complete guide that touches
all the issues related to security in Linux, from kernel security to
VPNs. Note that it has not been updated since 2001, but some information
is still relevant.
<footnote><para>
At a given time it was superseded by the 
"Linux Security Knowledge Base".
This documentation is also provided in
Debian through the <application>lskb</application> package. Now it's back
as the <emphasis>Lasg</emphasis> again.
</para></footnote></para></listitem>

<listitem><para> Kurt Seifried's <ulink
url="http://seifried.org/security/os/linux/20020324-securing-linux-step-by-step.html"
name="Securing Linux Step by Step" />.</para></listitem>

<listitem><para>In <ulink name="Securing and Optimizing Linux: RedHat Edition"
url="http://www.tldp.org/links/p_books.html#securing_linux" /> you
can find a similar document to this manual but related to Red Hat, some
of the issues are not distribution-specific and also apply to Debian.</para></listitem>

<listitem><para>Another Red Hat related document is <ulink name="EAL3 Evaluated Configuration 
Guide for Red Hat Enterprise" 
url="https://web.archive.org/web/20050520170309/https://ltp.sourceforge.net/docs/RHEL-EAL3-Configuration-Guide.pdf" />.</para></listitem>

<listitem><para>IntersectAlliance has published some documents that can be used
as reference cards on how to harden Linux servers (and their
services), the documents are available at <ulink
url="https://web.archive.org/web/20030210231943/http://www.intersectalliance.com/projects/index.html" name="their
site" />.</para></listitem>

<listitem><para>For network administrators, a good reference for building a
 secure network is the <ulink name="Securing your Domain HOWTO"
 url="https://web.archive.org/web/20030418093551/http://www.linuxsecurity.com/docs/LDP/Securing-Domain-HOWTO/" />.</para></listitem>

<listitem><para>If you want to evaluate the programs you are 
going to use (or want to build up some new ones) you 
should read the <ulink name="Secure Programs HOWTO"
url="http://www.tldp.org/HOWTO/Secure-Programs-HOWTO/" /> (master copy
is available at 
<ulink url="http://www.dwheeler.com/secure-programs/" />, it includes slides
and talks from the author, David Wheeler)</para></listitem>

<listitem><para>If you are considering installing firewall capabilities, you
should read the <ulink name="Firewall HOWTO"
url="http://www.tldp.org/HOWTO/Firewall-HOWTO.html" /> and the <ulink
name="IPCHAINS HOWTO"
url="http://www.tldp.org/HOWTO/IPCHAINS-HOWTO.html" /> (for kernels
previous to 2.4).</para></listitem>

<listitem><para>Finally, a good card to keep handy is the
<ulink name="Linux Security ReferenceCard"
 url="https://web.archive.org/web/20030308013020/http://www.linuxsecurity.com/docs/QuickRefCard.pdf" />.</para></listitem>

</itemizedlist>

<para>In any case, there is more information regarding the services
explained here (NFS, NIS, SMB...) in many of the HOWTOs of the <ulink
name="The Linux Documentation Project" url="http://www.tldp.org/" />. Some of 
these documents speak on the security side of a given service, so be sure to
take a look there too.</para>

<para>The HOWTO documents from the Linux Documentation Project are
available in Debian GNU/Linux through the installation of the
<package>doc-linux-text</package> (text version) or
<package>doc-linux-html</package> (HTML version). After installation
these documents will be available at the
<filename>/usr/share/doc/HOWTO/en-txt</filename> and
<filename>/usr/share/doc/HOWTO/en-html</filename> directories, respectively.</para>

<para>Other recommended Linux books:</para>

<itemizedlist>

<listitem><para>Maximum Linux Security : A Hacker's Guide to Protecting Your Linux
Server and Network. Anonymous. Paperback - 829 pages. Sams Publishing.
ISBN: 0672313413. July 1999.</para></listitem>

<listitem><para>Linux Security By John S. Flowers. New Riders; ISBN: 0735700354.
March 1999.</para></listitem>

<listitem><para><ulink url="https://web.archive.org/web/20030202131658/https://www.linux.org/books/ISBN_0072127732.html"
name="Hacking Linux Exposed" /> By Brian Hatch. McGraw-Hill Higher Education.
ISBN 0072127732. April, 2001</para></listitem>

</itemizedlist>

<para>Other books (which might be related to general issues
regarding UNIX and security and not Linux specific):</para>

<itemizedlist>

<listitem><para><ulink url="https://web.archive.org/web/20030206231652/http://www.oreilly.com/catalog/puis/"
name="Practical Unix and Internet Security (2nd Edition)" />
Garfinkel, Simpson, and Spafford, Gene; O'Reilly Associates;
ISBN 0-56592-148-8; 1004pp; 1996.</para></listitem>

<listitem><para>Firewalls and Internet Security Cheswick, William R. and Bellovin,
Steven M.; Addison-Wesley; 1994; ISBN 0-201-63357-4; 320pp.</para></listitem>

</itemizedlist>

<para>Some useful web sites to keep up to date regarding security:</para>

<itemizedlist>

<listitem><para><ulink name="NIST Security Guidelines"
url="http://csrc.nist.gov/" />.</para></listitem>

<listitem><para><ulink name="Security Focus" url="https://cve.mitre.org/data/refs/refmap/source-BUGTRAQ.html" />
	CVE Reference Map for Source BUGTRAQ</para></listitem>

<listitem><para> <ulink name="Linux Security"
	url="http://www.linuxsecurity.com/" />. General information
	regarding Linux security (tools, news...). Most useful is the
	<ulink name="main documentation"
	url="https://linuxsecurity.com/howtos" />
	page.</para></listitem>
</itemizedlist>
</section>

<section><title>How does Debian handle security?</title>
<para>Just so you have a general overview of security in Debian GNU/Linux
you should take note of the different issues that Debian tackles in
order to provide an overall secure system:</para>

<itemizedlist>
<listitem><para>Debian problems are always handled openly, even security
related. Security issues are discussed openly on the debian-security
mailing list. Debian Security Advisories (DSAs) are sent to public mailing
lists (both internal and external) and are published on the public
server. As the <ulink name="Debian Social Contract"
url="http://www.debian.org/social_contract" /> states:
<emphasis>We will not hide problems.
We will keep our entire bug report database open for public view
at all times. Reports that people file online will promptly become
visible to others.</emphasis></para></listitem>

<listitem><para>Debian follows security issues closely. The security team 
checks many security related sources, the most important being
<ulink name="Bugtraq" url="http://www.securityfocus.com/cgi-bin/vulns.pl" />,
on the lookout for packages with security issues that might be
included in Debian.</para></listitem>

<listitem><para>Security updates are the first priority. When a security problem
arises in a Debian package, the security update is prepared as fast
as possible and distributed for our stable, testing and unstable releases,
including all architectures.</para></listitem>

<listitem><para>Information regarding security is centralized in a single point,
<ulink url="http://security.debian.org/" />.</para></listitem>

<listitem><para>Debian is always trying to improve the overall security of the
distribution by starting new projects, such as automatic package signature 
verification mechanisms.</para></listitem>

<listitem><para>Debian provides a number of useful security related tools
for system administration and monitoring. Developers try to tightly
integrate these tools with the distribution in order to make them a better
suite to enforce local security policies. Tools include: integrity checkers, 
auditing tools, hardening tools, firewall tools, intrusion detection tools,
etc.</para></listitem>

<listitem><para>Package maintainers are aware of security issues. This leads
to many "secure by default" service installations which could
impose certain restrictions on their normal use. Debian does, however, try to
balance security and ease of administration - the programs are not de-activated
when you install them (as it is the case with say, the BSD family of
operating systems). In any case, prominent security issues (such as
<literal>setuid</literal> programs) are part of the
<ulink url="http://www.debian.org/doc/debian-policy/" name="Debian Policy" />.</para></listitem>

</itemizedlist>

<para>By publishing security information specific to Debian and complementing
other information-security documents related to Debian (see
<xref linkend="references" />), this document aims to produce better system
installations security-wise.</para>
</section>
</chapter>
