msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2024-05-19 13:26+0200\n"
"PO-Revision-Date: 2024-05-19 13:26+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: None\n"
"Language: en-US \n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Publican v4.3.2\n"

msgid "Security tools in Debian"
msgstr ""

msgid "FIXME: More content needed."
msgstr ""

msgid "Debian provides also a number of security tools that can make a Debian box suited for security purposes. These purposes include protection of information systems through firewalls (either packet or application-level), intrusion detection (both network and host based), vulnerability assessment, antivirus, private networks, etc."
msgstr ""

msgid "Since Debian 3.0 (<emphasis>woody</emphasis>), the distribution features cryptographic software integrated into the main distribution. OpenSSH and GNU Privacy Guard are included in the default install, and strong encryption is now present in web browsers and web servers, databases, and so forth. Further integration of cryptography is planned for future releases. This software, due to export restrictions in the US, was not distributed along with the main distribution but included only in non-US sites."
msgstr ""

msgid "Remote vulnerability assessment tools"
msgstr ""

msgid "The tools provided by Debian to perform remote vulnerability assessment are: <footnote><para> Some of them are provided when installing the <application>harden-remoteaudit</application> package. </para></footnote>"
msgstr ""

msgid "<application>nessus</application>"
msgstr ""

msgid "<application>raccess</application>"
msgstr ""

msgid "<application>nikto</application> (<command>whisker</command>'s replacement)"
msgstr ""

msgid "By far, the most complete and up-to-date tools is <application>nessus</application> which is composed of a client (<application>nessus</application>) used as a GUI and a server (<application>nessusd</application>) which launches the programmed attacks. Nessus includes remote vulnerabilities for quite a number of systems including network appliances, ftp servers, www servers, etc. The latest security plugins are able even to parse a web site and try to discover which interactive pages are available which could be attacked. There are also Java and Win32 clients (not included in Debian) which can be used to contact the management server."
msgstr ""

msgid "<application>nikto</application> is a web-only vulnerability assessment scanner including anti-IDS tactics (most of which are not <emphasis>anti-IDS</emphasis> anymore). It is one of the best cgi-scanners available, being able to detect a WWW server and launch only a given set of attacks against it. The database used for scanning can be easily modified to provide for new information."
msgstr ""

msgid "Network scanner tools"
msgstr ""

msgid "Debian does provide some tools used for remote scanning of hosts (but not vulnerability assessment). These tools are, in some cases, used by vulnerability assessment scanners as the first type of \"attack\" run against remote hosts in an attempt to determine remote services available. Currently Debian provides:"
msgstr ""

msgid "<application>nmap</application>"
msgstr ""

msgid "<application>xprobe</application>"
msgstr ""

msgid "<application>p0f</application>"
msgstr ""

msgid "<application>knocker</application>"
msgstr ""

msgid "<application>isic</application>"
msgstr ""

msgid "<application>hping2</application>"
msgstr ""

msgid "<application>icmpush</application>"
msgstr ""

msgid "<application>nbtscan</application> (for SMB /NetBIOS audits)"
msgstr ""

msgid "<application>fragrouter</application>"
msgstr ""

msgid "<command>strobe</command> (in the <application>netdiag</application> package)"
msgstr ""

msgid "<application>irpas</application>"
msgstr ""

msgid "While <application>xprobe</application> provide only remote operating system detection (using TCP/IP fingerprinting, <application>nmap</application> and <application>knocker</application> do both operating system detection and port scanning of the remote hosts. On the other hand, <application>hping2</application> and <application>icmpush</application> can be used for remote ICMP attack techniques."
msgstr ""

msgid "Designed specifically for SMB networks, <application>nbtscan</application> can be used to scan IP networks and retrieve name information from SMB-enabled servers, including: usernames, network names, MAC addresses..."
msgstr ""

msgid "On the other hand, <application>fragrouter</application> can be used to test network intrusion detection systems and see if the NIDS can be eluded by fragmentation attacks."
msgstr ""

msgid "FIXME: Check <ulink name=\"Bug #153117\" url=\"http://bugs.debian.org/153117\" /> (ITP fragrouter) to see if it's included."
msgstr ""

msgid "FIXME add information based on <ulink name=\"Debian Linux Laptop for Road Warriors\" url=\"https://web.archive.org/web/20040725013857/http://www.giac.org/practical/gcux/Stephanie_Thomas_GCUX.pdf\" /> which describes how to use Debian and a laptop to scan for wireless (803.1) networks (link not there any more)."
msgstr ""

msgid "Internal audits"
msgstr ""

msgid "Currently, only the <application>tiger</application> tool used in Debian can be used to perform internal (also called white box) audit of hosts in order to determine if the file system is properly set up, which processes are listening on the host, etc."
msgstr ""

msgid "Auditing source code"
msgstr ""

msgid "Debian provides several packages that can be used to audit C/C++ source code programs and find programming errors that might lead to potential security flaws:"
msgstr ""

msgid "<application>flawfinder</application>"
msgstr ""

msgid "<application>rats</application>"
msgstr ""

msgid "<application>splint</application>"
msgstr ""

msgid "<application>pscan</application>"
msgstr ""

msgid "Virtual Private Networks"
msgstr ""

msgid "A virtual private network (VPN) is a group of two or more computer systems, typically connected to a private network with limited public network access, that communicate securely over a public network. VPNs may connect a single computer to a private network (client-server), or a remote LAN to a private network (server-server). VPNs often include the use of encryption, strong authentication of remote users or hosts, and methods for hiding the private network's topology."
msgstr ""

msgid "Debian provides quite a few packages to set up encrypted virtual private networks:"
msgstr ""

msgid "<application>vtun</application>"
msgstr ""

msgid "<application>tunnelv</application> (non-US section)"
msgstr ""

msgid "<application>cipe-source</application>, <application>cipe-common</application>"
msgstr ""

msgid "<application>tinc</application>"
msgstr ""

msgid "<application>secvpn</application>"
msgstr ""

msgid "<application>pptpd</application>"
msgstr ""

msgid "<application>openvpn</application>"
msgstr ""

msgid "<application>openswan</application> (<ulink url=\"http://www.openswan.org/\" />)"
msgstr ""

msgid "FIXME: Update the information here since it was written with FreeSWAN in mind. Check Bug #237764 and Message-Id: &lt;200412101215.04040.rmayr@debian.org&gt;."
msgstr ""

msgid "The OpenSWAN package is probably the best choice overall, since it promises to interoperate with almost anything that uses the IP security protocol, IPsec (RFC 2411). However, the other packages listed above can also help you get a secure tunnel up in a hurry. The point to point tunneling protocol (PPTP) is a proprietary Microsoft protocol for VPN. It is supported under Linux, but is known to have serious security issues."
msgstr ""

msgid "For more information see the <ulink name=\"VPN-Masquerade HOWTO\" url=\"http://www.tldp.org/HOWTO/VPN-Masquerade-HOWTO.html\" /> (covers IPsec and PPTP), <ulink name=\"VPN HOWTO\" url=\"http://www.tldp.org/HOWTO/VPN-HOWTO.html\" /> (covers PPP over SSH), <ulink name=\"Cipe mini-HOWTO\" url=\"http://www.tldp.org/HOWTO/mini/Cipe+Masq.html\" />, and <ulink name=\"PPP and SSH mini-HOWTO\" url=\"http://www.tldp.org/HOWTO/mini/ppp-ssh/index.html\" />."
msgstr ""

msgid "Also worth checking out is <ulink name=\"Yavipin\" url=\"http://yavipin.sourceforge.net/\" />, but no Debian packages seem to be available yet."
msgstr ""

msgid "Point to Point tunneling"
msgstr ""

msgid "If you want to provide a tunneling server for a mixed environment (both Microsoft operating systems and Linux clients) and IPsec is not an option (since it's only provided for Windows 2000 and Windows XP), you can use <emphasis>PoPToP</emphasis> (Point to Point Tunneling Server), provided in the <application>pptpd</application> package."
msgstr ""

msgid "If you want to use Microsoft's authentication and encryption with the server provided in the <application>ppp</application> package, note the following from the FAQ:"
msgstr ""

msgid ""
"\n"
"It is only necessary to use PPP 2.3.8 if you want Microsoft compatible\n"
"MSCHAPv2/MPPE authentication and encryption. The reason for this is that\n"
"the MSCHAPv2/MPPE patch currently supplied (19990813) is against PPP\n"
"2.3.8. If you don't need Microsoft compatible authentication/encryption\n"
"any 2.3.x PPP source will be fine."
msgstr ""

msgid "However, you also have to apply the kernel patch provided by the <application>kernel-patch-mppe</application> package, which provides the pp_mppe module for pppd."
msgstr ""

msgid "Take into account that the encryption in ppptp forces you to store user passwords in clear text, and that the MS-CHAPv2 protocol contains <ulink name=\"known security holes\" url=\"http://mopo.informatik.uni-freiburg.de/pptp_mschapv2/\" />."
msgstr ""

msgid "Public Key Infrastructure (PKI)"
msgstr ""

msgid "Public Key Infrastructure (PKI) is a security architecture introduced to provide an increased level of confidence for exchanging information over insecure networks. It makes use of the concept of public and private cryptographic keys to verify the identity of the sender (signing) and to ensure privacy (encryption)."
msgstr ""

msgid "When considering a PKI, you are confronted with a wide variety of issues:"
msgstr ""

msgid "a Certificate Authority (CA) that can issue and verify certificates, and that can work under a given hierarchy."
msgstr ""

msgid "a Directory to hold user's public certificates."
msgstr ""

msgid "a Database (?) to maintain Certificate Revocation Lists (CRL)."
msgstr ""

msgid "devices that interoperate with the CA in order to print out smart cards/USB tokens/whatever to securely store certificates."
msgstr ""

msgid "certificate-aware applications that can use certificates issued by a CA to enroll in encrypted communication and check given certificates against CRL (for authentication and full Single Sign On solutions)."
msgstr ""

msgid "a Time stamping authority to digitally sign documents."
msgstr ""

msgid "a management console from which all of this can be properly used (certificate generation, revocation list control, etc...)."
msgstr ""

msgid "Debian GNU/Linux has software packages to help you with some of these PKI issues. They include <command>OpenSSL</command> (for certificate generation), <command>OpenLDAP</command> (as a directory to hold the certificates), <command>gnupg</command> and <command>openswan</command> (with X.509 standard support). However, as of the Woody release (Debian 3.0), Debian does not have any of the freely available Certificate Authorities such as pyCA, <ulink name=\"OpenCA\" url=\"http://www.openca.org\" /> or the CA samples from OpenSSL. For more information read the <ulink name=\"Open PKI book\" url=\"http://ospkibook.sourceforge.net/\" />."
msgstr ""

msgid "SSL Infrastructure"
msgstr ""

msgid "Debian does provide some SSL certificates with the distribution so that they can be installed locally. They are found in the <application>ca-certificates</application> package. This package provides a central repository of certificates that have been submitted to Debian and approved (that is, verified) by the package maintainer, useful for any OpenSSL applications which verify SSL connections."
msgstr ""

msgid "FIXME: read debian-devel to see if there was something added to this."
msgstr ""

msgid "Antivirus tools"
msgstr ""

msgid "There are not many anti-virus tools included with Debian GNU/Linux, probably because GNU/Linux users are not plagued by viruses. The Unix security model makes a distinction between privileged (root) processes and user-owned processes, therefore a \"hostile\" executable that a non-root user receives or creates and then executes cannot \"infect\" or otherwise manipulate the whole system. However, GNU/Linux worms and viruses do exist, although there has not (yet, hopefully) been any that has spread in the wild over any Debian distribution. In any case, administrators might want to build up anti-virus gateways that protect against viruses arising on other, more vulnerable systems in their network."
msgstr ""

msgid "Debian GNU/Linux currently provides the following tools for building antivirus environments:"
msgstr ""

msgid "<ulink name=\"Clam Antivirus\" url=\"http://www.clamav.net\" />, provided since Debian <emphasis>sarge</emphasis> (3.1 release). Packages are provided both for the virus scanner (<application>clamav</application>) for the scanner daemon (<application>clamav-daemon</application>) and for the data files needed for the scanner. Since keeping an antivirus up-to-date is critical for it to work properly there are two different ways to get this data: <application>clamav-freshclam</application> provides a way to update the database through the Internet automatically and <application>clamav-data</application> which provides the data files directly. <footnote><para>If you use this last package and are running an official Debian, the database will not be updated with security updates. You should either use <application>clamav-freshclam</application>, <command>clamav-getfiles</command> to generate new <application>clamav-data</application> packages or update from the maintainers location: <screen> deb http://people.debian.org/~zugschlus/clamav-data/ / deb-src http://people.debian.org/~zugschlus/clamav-data/ / </screen> </para></footnote>"
msgstr ""

msgid "<application>mailscanner</application> an e-mail gateway virus scanner and spam detector. Using <application>sendmail</application> or <application>exim</application> as its basis, it can use more than 17 different virus scanning engines (including <application>clamav</application>)."
msgstr ""

msgid "<application>libfile-scan-perl</application> which provides File::Scan, a Perl extension for scanning files for viruses. This modules can be used to make platform independent virus scanners."
msgstr ""

msgid "<ulink name=\"Amavis Next Generation\" url=\"http://www.sourceforge.net/projects/amavis\" />, provided in the package <application>amavis-ng</application> and available in <emphasis>sarge</emphasis>, which is a mail virus scanner which integrates with different MTA (Exim, Sendmail, Postfix, or Qmail) and supports over 15 virus scanning engines (including clamav, File::Scan and openantivirus)."
msgstr ""

msgid "<ulink name=\"sanitizer\" url=\"http://packages.debian.org/sanitizer\" />, a tool that uses the <application>procmail</application> package, which can scan email attachments for viruses, block attachments based on their filenames, and more."
msgstr ""

msgid "<ulink name=\"amavis-postfix\" url=\"http://packages.debian.org/amavis-postfix\" />, a script that provides an interface from a mail transport agent to one or more commercial virus scanners (this package is built with support for the <command>postfix</command> MTA only)."
msgstr ""

msgid "<application>exiscan</application>, an e-mail virus scanner written in Perl that works with Exim."
msgstr ""

msgid "<application>blackhole-qmail</application> a spam filter for Qmail with built-in support for Clamav."
msgstr ""

msgid "Some gateway daemons support already tools extensions to build antivirus environments including <application>exim4-daemon-heavy</application> (the <emphasis>heavy</emphasis> version of the Exim MTA), <application>frox</application> (a transparent caching ftp proxy server), <application>messagewall</application> (an SMTP proxy daemon) and <application>pop3vscan</application> (a transparent POP3 proxy)."
msgstr ""

msgid "Debian currently provide <command>clamav</command> as the only antivirus scanning software in the main official distribution and it also provides multiple interfaces to build gateways with antivirus capabilities for different protocols."
msgstr ""

msgid "Some other free software antivirus projects which might be included in future Debian GNU/Linux releases:<ulink name=\"Open Antivirus\" url=\"http://sourceforge.net/projects/openantivirus/\" /> (see <ulink name=\"Bug #150698 (ITP oav-scannerdaemon)\" url=\"http://bugs.debian.org/150698\" /> and <ulink name=\"Bug #150695 (ITP oav-update)\" url=\"http://bugs.debian.org/150695\" /> )."
msgstr ""

msgid "FIXME: Is there a package that provides a script to download the latest virus signatures from <ulink url=\"http://www.openantivirus.org/latest.php\" />?"
msgstr ""

msgid "FIXME: Check if scannerdaemon is the same as the open antivirus scanner daemon (read ITPs)."
msgstr ""

msgid "However, Debian will <emphasis>never</emphasis> provide propietary (non-free and undistributable) antivirus software such as: Panda Antivirus, NAI Netshield, <ulink name=\"Sophos Sweep\" url=\"http://www.sophos.com/\" />, <ulink name=\"TrendMicro Interscan\" url=\"http://www.antivirus.com\" />, or <ulink name=\"RAV\" url=\"http://www.ravantivirus.com\" />. For more pointers see the <ulink name=\"Linux antivirus software mini-FAQ\" url=\"http://www.computer-networking.de/~link/security/av-linux_e.txt\" />. This does not mean that this software cannot be installed properly in a Debian system<footnote><para> Actually, there is an installer package for the <emphasis>F-prot</emphasis> antivirus, which is non-free but <emphasis>gratis</emphasis> for home users, called <command>f-prot-installer</command>. This installer, however, just downloads <ulink name=\"F-prot's software\" url=\"http://www.f-prot.com/products/home_use/linux/\" /> and installs it in the system.</para></footnote>."
msgstr ""

msgid "For more information on how to set up a virus detection system read Dave Jones' article <ulink name=\"Building an E-mail Virus Detection System for Your Network\" url=\"https://web.archive.org/web/20120509212938/http://www.linuxjournal.com/article/4882\" />."
msgstr ""

msgid "GPG agent"
msgstr ""

msgid "It is very common nowadays to digitally sign (and sometimes encrypt) e-mail. You might, for example, find that many people participating on mailing lists sign their list e-mail. Public key signatures are currently the only means to verify that an e-mail was sent by the sender and not by some other person."
msgstr ""

msgid "Debian GNU/Linux provides a number of e-mail clients with built-in e-mail signing capabilities that interoperate either with <application>gnupg</application> or <application>pgp</application>:"
msgstr ""

msgid "<application>evolution</application>."
msgstr ""

msgid "<application>mutt</application>."
msgstr ""

msgid "<application>kmail</application>."
msgstr ""

msgid "<application>icedove</application> (rebranded version of Mozilla's Thunderbird) through the <ulink name=\"Enigmail\" url=\"http://enigmail.mozdev.org/\" /> plugin. This plugin is provided by the <application>enigmail</application> package."
msgstr ""

msgid "<application>sylpheed</application>. Depending on how the stable version of this package evolves, you may need to use the <emphasis>bleeding edge version</emphasis>, <application>sylpheed-claws</application>."
msgstr ""

msgid "<application>gnus</application>, which when installed with the <application>mailcrypt</application> package, is an <command>emacs</command> interface to <command>gnupg</command>."
msgstr ""

msgid "<application>kuvert</application>, which provides this functionality independently of your chosen mail user agent (MUA) by interacting with the mail transport agent (MTA)."
msgstr ""

msgid "Key servers allow you to download published public keys so that you may verify signatures. One such key server is <ulink url=\"http://wwwkeys.pgp.net\" />. <application>gnupg</application> can automatically fetch public keys that are not already in your public keyring. For example, to configure <command>gnupg</command> to use the above key server, edit the file <filename>~/.gnupg/options</filename> and add the following line: <footnote><para> For more examples of how to configure <command>gnupg</command> check <filename>/usr/share/doc/mutt/examples/gpg.rc</filename>. </para></footnote>"
msgstr ""

msgid "\n"
"keyserver wwwkeys.pgp.net"
msgstr ""

msgid "Most key servers are linked, so that when your public key is added to one server, the addition is propagated to all the other public key servers. There is also a Debian GNU/Linux package <application>debian-keyring</application>, that provides all the public keys of the Debian developers. The <command>gnupg</command> keyrings are installed in <filename>/usr/share/keyrings/</filename>."
msgstr ""

msgid "For more information:"
msgstr ""

msgid "<ulink name=\"GnuPG FAQ\" url=\"http://www.gnupg.org/faq.html\" />."
msgstr ""

msgid "<ulink name=\"GnuPG Handbook\" url=\"http://www.gnupg.org/gph/en/manual.html\" />."
msgstr ""

msgid "<ulink name=\"GnuPG Mini Howto (English)\" url=\"https://web.archive.org/web/20080201103530/http://www.dewinter.com/gnupg_howto/english/GPGMiniHowto.html\" />."
msgstr ""

msgid "<ulink name=\"comp.security.pgp FAQ\" url=\"https://web.archive.org/web/20080513095235/http://www.uk.pgp.net/pgpnet/pgp-faq/\" />."
msgstr ""

msgid "<ulink name=\"Keysigning Party HOWTO\" url=\"https://web.archive.org/web/20060222110131/http://www.cryptnet.net/fdp/crypto/gpg-party.html\" />."
msgstr ""

